import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatCheckboxChange, MatSelectionList, MatListOption, MatDrawer } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { }
