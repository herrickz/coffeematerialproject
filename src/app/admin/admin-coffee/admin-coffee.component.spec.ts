import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCoffeeComponent } from './admin-coffee.component';

describe('AdminCoffeeComponent', () => {
  let component: AdminCoffeeComponent;
  let fixture: ComponentFixture<AdminCoffeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCoffeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCoffeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
