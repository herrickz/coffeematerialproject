import { Component, OnInit } from '@angular/core';
import { Coffee } from '../../core/models/coffee.model';
import { CoffeeService } from '../../core/services/coffee.service';

@Component({
  selector: 'app-admin-coffee',
  templateUrl: './admin-coffee.component.html',
  styleUrls: ['./admin-coffee.component.css']
})
export class AdminCoffeeComponent implements OnInit {

  coffees: Coffee[];

  creatingCoffee: boolean = false;
  coffeeToCreate = {
    name: "",
    region: ""
  };

  constructor(private coffeeService: CoffeeService) { }

  ngOnInit() {
    this.coffeeService.getCoffees().subscribe((coffees) => {
      this.coffees = coffees;
    }, (error) => {

    });
  }

  createCoffee(): void {
    this.creatingCoffee = true;
  }

  addCoffee(): void {
    this.coffeeService.createCoffee(this.coffeeToCreate).subscribe((coffee) => {
      this.creatingCoffee = false;
      this.coffeeToCreate = {
        name: "",
        region: ""
      }
      this.coffees.push(coffee);
    }, (error) => {
      this.creatingCoffee = false;
    });
  }

  cancelCreateCoffee() {
    this.coffeeToCreate = {
      name: "",
      region: ""
    }

    this.creatingCoffee = false;
  }

  deleteCoffee(coffee: Coffee) {
    this.coffeeService.deleteCoffee(coffee).subscribe((data) => {
      
    }, (error) => {

    });
  }

}
