import { Component, OnInit } from '@angular/core';
import { LoginService } from '../core/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = "";
  password: string = "";

  loginPending: boolean = false;

  constructor(private loginService: LoginService) { }

  ngOnInit() { }

  login() {
    this.loginPending = true;
    let data = {
      username: this.username,
      password: this.password
    }
    this.loginService.login(data).subscribe((token) => {
      this.loginService.setToken(token["token"]);
      this.loginPending = false;
    }, (error) => {
      this.loginPending = false;
    });
  }

}
