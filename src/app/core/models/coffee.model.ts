
export interface Coffee {
    id: number;
    name: string;
    region: string;
}