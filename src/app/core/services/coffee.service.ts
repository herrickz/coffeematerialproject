import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Coffee } from "../models/coffee.model";

@Injectable()
export class CoffeeService {

  url: string = "http://localhost:8000/api/coffee/";
  
  constructor(private http: HttpClient) {}

  getCoffees(): Observable<Coffee[]> {
    return this.http.get<Coffee[]>(this.url);
  }

  createCoffee(coffee): Observable<Coffee>{
    return this.http.post<Coffee>(this.url, coffee);
  }

  deleteCoffee(coffee: Coffee) {
    return this.http.delete(`${this.url}${coffee.id}/`);
  }
}
