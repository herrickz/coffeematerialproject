import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  login(usernamePass: any) {
    let url = "http://localhost:8000/auth/token/";
    return this.http.post(url, usernamePass);
  }

  setToken(token: any) {
    document.cookie = `token=${token}`;

    console.log(document.cookie);
  }

  getToken() {

  }

}
