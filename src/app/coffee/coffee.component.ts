import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSelectionList, MatDrawer, MatListOption } from '@angular/material';
import { CoffeeService } from '../core/services/coffee.service';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent implements OnInit {

  ngOnInit() {
    
  }

  showFiller = false;
  title = 'app';
  filterIcon: string = "keyboard_arrow_right";

  coffees = [
    {
      name: "Ethiopia Good Stuff",
      region: "Ethiopia",
      visible: true
    }, 
    {
      name: "Berundi",
      region: "Tanzania",
      visible: true
    }, 
    {
      name: "Guatemala",
      region: "Guatemala",
      visible: true
    }, 
    {
      name: "Costa Rica",
      region: "Costa Rica",
      visible: true
    }
  ];

  constructor(private coffeeService: CoffeeService) { }

  @ViewChild(MatSelectionList) regionCheckBoxList: MatSelectionList;
  @ViewChild(MatDrawer) filterMenu: MatDrawer;

  toggleFilterMenu() {
    this.filterIcon = this.filterIcon === "keyboard_arrow_right" ? "keyboard_arrow_left" : "keyboard_arrow_right";
    this.filterMenu.toggle();
  }

  regionFilterChange() {
    
    let regionSelection = this.regionCheckBoxList.selectedOptions.selected;

    if(regionSelection.length === 0) {
      this.enableAllCoffees();
    }
    else {
      this.filterCoffeeSelection(regionSelection);
    }
    
  }

  private filterCoffeeSelection(regionSelection: MatListOption[]) {
    let regionArr: string[] = [];
    for (let selected of regionSelection) {
      regionArr.push(selected.getLabel().trim());
    }
    for (let coffee of this.coffees) {
      if (!regionArr.includes(coffee.region)) {
        coffee.visible = false;
      }
      else {
        coffee.visible = true;
      }
    }
  }

  enableAllCoffees(): void {
    for(let coffee of this.coffees) {
      coffee.visible = true;
    }
  }

}
